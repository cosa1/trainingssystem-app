
import Vue from 'vue'
import VueRouter from 'vue-router'

import interventionen from './views/interventionen.vue'
import intervention from './views/intervention.vue'
import verlauf from './views/verlauf.vue'
import account from './views/account.vue'
import impressum from './views/impressum.vue'
import home from './views/home.vue'
import eintrag from './views/eintrag.vue'
import login from './views/login.vue'
import store from './store.js'


Vue.use(VueRouter)

export const router = new VueRouter({
  mode: 'history',
  base: '/pwa-test',
  relative: true,
  routes: [
    {
      path: '/',
      component: home,
      props: true,
      meta: {
        title: 'Care4Care',
        name: 'Start',
        level: 1,
        AuthRequired: true
      },
    },
    {
      path: '/login',
      component: login,
      props: true,
      meta: {
        title: 'Willkommen',
        level: 1
      },
    },
    {
      path: '/interventionen',
      component: interventionen,
      props: true,
      meta: {
        title: 'Interventionen',
        level: 1,
        AuthRequired: true
      },
    },
    {
      path: '/verlauf',
      component: verlauf,
      props: true,
      meta: {
        title: 'Verlauf',
        level: 1,
        AuthRequired: true
      },
    },
    {
      path: '/account',
      component: account,
      props: true,
      meta: {
        title: 'Account',
        level: 1,
        AuthRequired: true
      },
    },
    {
      path: '/impressum',
      component: impressum,
      props: true,
      meta: { 
        title: 'Impressum',
        level: 1,
        AuthRequired: false
       },
    },
    {
      path: '/eintrag',
      component: eintrag,
      props: true,
      name: 'eintrag',
      meta: { 
        title: 'Eintrag', 
        level: 2,
        AuthRequired: true
       },
    },
    {
      path: '/intervention',
      component: intervention,
      props: true,
      name: 'intervention',
      meta: { 
        title: 'Intervention', 
        level: 2,
        AuthRequired: true
       },
    }
  ]
})


router.beforeEach(async (to, from, next) => {
  
  if (to.matched.some(record => record.meta.AuthRequired)) {
    // this route requires auth, check if user is logged in
    // if not, redirect to login page.
    if (!store.getters.isLoggedIn) {
       next({
        path: '/login',
         query: { redirect: to.fullPath }
       })
    } else {
      // we have a state.user object but
      // we need to check if the token is still valid
      try{
        //const { status } = await $store.dispatch('validate')
        // user is logged in with a valid token
        next()
      }catch(e){
        // the token is invalid so we will have the user login again
        // clear the token and user info
        //store.commit('DELETE_USER')
        next({
          path: '/login',
          query: { redirect: to.fullPath }
        })
      }
    }
  } else {
    // this is not a protected route
    next()
  }
})
