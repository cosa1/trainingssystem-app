import Vue from 'vue'
import Vuex from 'vuex'
import {router} from './routes'

Vue.use(Vuex)


  export default new Vuex.Store({
    state: {
          token: null,
          isLoggedIn:true,
          user:null,
      userData:{
        user_name: null,
        user_id:null,
        user_email:null
      }
    },
    mutations: {
      SET_USER(state, user){
        state.userData.user_name = user;
      },
      SET_TOKEN(state, token){
        state.token = token;
      },
      SET_SESSION(state, payload){
        state.isLoggedIn = payload;
      },
      setUser(state, username){
        state.user = username
        console.log("setUser called");
    },
    },
    actions: {
      UserSetData({commit}, user){
        if(user.username=='demo@demo.de' && user.password=='demo'){
          commit ('SET_USER', user.username); 
          commit('SET_SESSION', true);
          console.log('logged in!')
          router.push({ path: "/"});
        }else{
          console.log('Username oder Passwort falsch!')
        }     
      },
      UserSetToken({commit}, token){
        commit ('SET_TOKEN', token.token); 
      },
      Logout({commit}){
        commit('SET_SESSION', false);
        commit('SET_USER', null);
        commit ('SET_TOKEN', null); 
        router.push({ path: "/login"});
      }
    },
    getters:{
      isLoggedIn(state){
        return state.isLoggedIn;
      },
      getUserName(state){
        return state.userData.user_name;
      }
    }
  });
  
